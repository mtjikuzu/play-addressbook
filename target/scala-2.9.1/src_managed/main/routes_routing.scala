// @SOURCE:C:/Users/mtjikuzu/workspace/addressbook/conf/routes
// @HASH:a55b2a5d2cf4c3f7b18ef3a2d3ed9f044ae13212
// @DATE:Mon May 14 00:23:11 CDT 2012


import play.core._
import play.core.Router._
import play.core.j._

import play.api.mvc._


import Router.queryString

object Routes extends Router.Routes {

private var _prefix = "/"

def setPrefix(prefix: String) {
  _prefix = prefix  
  List[(String,Routes)]().foreach {
    case (p, router) => router.setPrefix(prefix + (if(prefix.endsWith("/")) "" else "/") + p)
  }
}

def prefix = _prefix


// @LINE:6
lazy val controllers_Application_index0 = Route("GET", PathPattern(List(StaticPart(Routes.prefix))))
          

// @LINE:8
lazy val controllers_Application_listContacts1 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(if(Routes.prefix.endsWith("/")) "" else "/"),StaticPart("contacts"))))
          

// @LINE:10
lazy val controllers_Application_createContact2 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(if(Routes.prefix.endsWith("/")) "" else "/"),StaticPart("contacts"))))
          

// @LINE:12
lazy val controllers_Application_deleteContact3 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(if(Routes.prefix.endsWith("/")) "" else "/"),StaticPart("contacts/"),DynamicPart("id", """[^/]+"""),StaticPart("/delete"))))
          

// @LINE:15
lazy val controllers_Assets_at4 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(if(Routes.prefix.endsWith("/")) "" else "/"),StaticPart("assets/"),DynamicPart("file", """.+"""))))
          
def documentation = List(("""GET""", prefix,"""controllers.Application.index"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """contacts""","""controllers.Application.listContacts"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """contacts""","""controllers.Application.createContact"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """contacts/$id<[^/]+>/delete""","""controllers.Application.deleteContact(id:Long)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """assets/$file<.+>""","""controllers.Assets.at(path:String = "/public", file:String)""")).foldLeft(List.empty[(String,String,String)]) { (s,e) => e match {
  case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
  case l => s ++ l.asInstanceOf[List[(String,String,String)]] 
}}
         
    
def routes:PartialFunction[RequestHeader,Handler] = {        

// @LINE:6
case controllers_Application_index0(params) => {
   call { 
        invokeHandler(_root_.controllers.Application.index, HandlerDef(this, "controllers.Application", "index", Nil))
   }
}
          

// @LINE:8
case controllers_Application_listContacts1(params) => {
   call { 
        invokeHandler(_root_.controllers.Application.listContacts, HandlerDef(this, "controllers.Application", "listContacts", Nil))
   }
}
          

// @LINE:10
case controllers_Application_createContact2(params) => {
   call { 
        invokeHandler(_root_.controllers.Application.createContact, HandlerDef(this, "controllers.Application", "createContact", Nil))
   }
}
          

// @LINE:12
case controllers_Application_deleteContact3(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(_root_.controllers.Application.deleteContact(id), HandlerDef(this, "controllers.Application", "deleteContact", Seq(classOf[Long])))
   }
}
          

// @LINE:15
case controllers_Assets_at4(params) => {
   call(Param[String]("path", Right("/public")), params.fromPath[String]("file", None)) { (path, file) =>
        invokeHandler(_root_.controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String])))
   }
}
          
}
    
}
          