// @SOURCE:C:/Users/mtjikuzu/workspace/addressbook/conf/routes
// @HASH:a55b2a5d2cf4c3f7b18ef3a2d3ed9f044ae13212
// @DATE:Mon May 14 00:23:11 CDT 2012


import play.core._
import play.core.Router._
import play.core.j._

import play.api.mvc._


import Router.queryString


// @LINE:15
// @LINE:12
// @LINE:10
// @LINE:8
// @LINE:6
package controllers {

// @LINE:12
// @LINE:10
// @LINE:8
// @LINE:6
class ReverseApplication {
    


 
// @LINE:12
def deleteContact(id:Long) = {
   Call("GET", Routes.prefix + { if(Routes.prefix.endsWith("/")) "" else "/" } + "contacts/" + implicitly[PathBindable[Long]].unbind("id", id) + "/delete")
}
                                                        
 
// @LINE:6
def index() = {
   Call("GET", Routes.prefix)
}
                                                        
 
// @LINE:8
def listContacts() = {
   Call("GET", Routes.prefix + { if(Routes.prefix.endsWith("/")) "" else "/" } + "contacts")
}
                                                        
 
// @LINE:10
def createContact() = {
   Call("POST", Routes.prefix + { if(Routes.prefix.endsWith("/")) "" else "/" } + "contacts")
}
                                                        

                      
    
}
                            

// @LINE:15
class ReverseAssets {
    


 
// @LINE:15
def at(file:String) = {
   Call("GET", Routes.prefix + { if(Routes.prefix.endsWith("/")) "" else "/" } + "assets/" + implicitly[PathBindable[String]].unbind("file", file))
}
                                                        

                      
    
}
                            
}
                    


// @LINE:15
// @LINE:12
// @LINE:10
// @LINE:8
// @LINE:6
package controllers.javascript {

// @LINE:12
// @LINE:10
// @LINE:8
// @LINE:6
class ReverseApplication {
    


 
// @LINE:12
def deleteContact = JavascriptReverseRoute(
   "controllers.Application.deleteContact",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + Routes.prefix + { if(Routes.prefix.endsWith("/")) "" else "/" } + """" + "contacts/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id) + "/delete"})
      }
   """
)
                                
 
// @LINE:6
def index = JavascriptReverseRoute(
   "controllers.Application.index",
   """
      function() {
      return _wA({method:"GET", url:"""" + Routes.prefix + """"})
      }
   """
)
                                
 
// @LINE:8
def listContacts = JavascriptReverseRoute(
   "controllers.Application.listContacts",
   """
      function() {
      return _wA({method:"GET", url:"""" + Routes.prefix + { if(Routes.prefix.endsWith("/")) "" else "/" } + """" + "contacts"})
      }
   """
)
                                
 
// @LINE:10
def createContact = JavascriptReverseRoute(
   "controllers.Application.createContact",
   """
      function() {
      return _wA({method:"POST", url:"""" + Routes.prefix + { if(Routes.prefix.endsWith("/")) "" else "/" } + """" + "contacts"})
      }
   """
)
                                

                      
    
}
                

// @LINE:15
class ReverseAssets {
    


 
// @LINE:15
def at = JavascriptReverseRoute(
   "controllers.Assets.at",
   """
      function(file) {
      return _wA({method:"GET", url:"""" + Routes.prefix + { if(Routes.prefix.endsWith("/")) "" else "/" } + """" + "assets/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("file", file)})
      }
   """
)
                                

                      
    
}
                
}
          


// @LINE:15
// @LINE:12
// @LINE:10
// @LINE:8
// @LINE:6
package controllers.ref {

// @LINE:12
// @LINE:10
// @LINE:8
// @LINE:6
class ReverseApplication {
    


 
// @LINE:12
def deleteContact(id:Long) = new play.api.mvc.HandlerRef(
   controllers.Application.deleteContact(id), HandlerDef(this, "controllers.Application", "deleteContact", Seq(classOf[Long]))
)
                              
 
// @LINE:6
def index() = new play.api.mvc.HandlerRef(
   controllers.Application.index(), HandlerDef(this, "controllers.Application", "index", Seq())
)
                              
 
// @LINE:8
def listContacts() = new play.api.mvc.HandlerRef(
   controllers.Application.listContacts(), HandlerDef(this, "controllers.Application", "listContacts", Seq())
)
                              
 
// @LINE:10
def createContact() = new play.api.mvc.HandlerRef(
   controllers.Application.createContact(), HandlerDef(this, "controllers.Application", "createContact", Seq())
)
                              

                      
    
}
                            

// @LINE:15
class ReverseAssets {
    


 
// @LINE:15
def at(path:String, file:String) = new play.api.mvc.HandlerRef(
   controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]))
)
                              

                      
    
}
                            
}
                    
        