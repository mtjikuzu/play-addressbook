
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
/**/
object index extends BaseScalaTemplate[play.api.templates.Html,Format[play.api.templates.Html]](play.api.templates.HtmlFormat) with play.api.templates.Template2[List[Contact],Form[String],play.api.templates.Html] {

    /**/
    def apply/*1.2*/(contacts: List[Contact], contactForm: Form[String]):play.api.templates.Html = {
        _display_ {import helper._


Seq[Any](format.raw/*1.54*/("""

"""),_display_(Seq[Any](/*4.2*/main("Contacts")/*4.18*/ {_display_(Seq[Any](format.raw/*4.20*/("""
    
   """),_display_(Seq[Any](/*6.5*/form(routes.Application.createContact())/*6.45*/{_display_(Seq[Any](format.raw/*6.46*/("""
   """),_display_(Seq[Any](/*7.5*/inputText(contactForm("contact"), '_label -> "Name"))),format.raw/*7.57*/("""
   <input type="submit" value="Create">
""")))})),format.raw/*9.2*/("""

<h3>"""),_display_(Seq[Any](/*11.6*/contacts/*11.14*/.size)),format.raw/*11.19*/(""" Contact(s)</h3>

<ul>
	"""),_display_(Seq[Any](/*14.3*/for(contact <- contacts) yield /*14.27*/{_display_(Seq[Any](format.raw/*14.28*/("""
	<li>"""),_display_(Seq[Any](/*15.7*/contact/*15.14*/.contact)),format.raw/*15.22*/("""
		<a href=""""),_display_(Seq[Any](/*16.13*/routes/*16.19*/.Application.deleteContact(contact.id))),format.raw/*16.57*/("""">Delete Contact</a>
	</li>
""")))})),format.raw/*18.2*/("""

</ul>
    
""")))})))}
    }
    
    def render(contacts:List[Contact],contactForm:Form[String]) = apply(contacts,contactForm)
    
    def f:((List[Contact],Form[String]) => play.api.templates.Html) = (contacts,contactForm) => apply(contacts,contactForm)
    
    def ref = this

}
                /*
                    -- GENERATED --
                    DATE: Mon May 14 00:31:28 CDT 2012
                    SOURCE: C:/Users/mtjikuzu/workspace/addressbook/app/views/index.scala.html
                    HASH: d7cbcfa04d4621664eca920155bb5d4409ddcf23
                    MATRIX: 536->1|683->53|722->76|746->92|785->94|831->106|879->146|917->147|957->153|1030->205|1104->249|1148->258|1165->266|1192->271|1255->299|1295->323|1334->324|1377->332|1393->339|1423->347|1473->361|1488->367|1548->405|1610->436
                    LINES: 19->1|23->1|25->4|25->4|25->4|27->6|27->6|27->6|28->7|28->7|30->9|32->11|32->11|32->11|35->14|35->14|35->14|36->15|36->15|36->15|37->16|37->16|37->16|39->18
                    -- GENERATED --
                */
            