package models

import play.api._
import play.api.mvc._
import scala.collection.mutable.HashMap

case class Contact(id: Long, contact: String)

object Contact{	
	
	val contacts = new HashMap[Long, Contact]
	var id = 0L

	def nexttId():Long = {id += 1; id}

	def create(contact: String) = {val id = nexttId; contacts += id -> Contact(id, contact)}

	def delete(id: Long) = {contacts -= id}

	def list(): List[Contact] = {contacts.values.toList}
}